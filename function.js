function sum(a, b) {
    console.log(a + b);
}
sum(10, 20);

var sum = function (a, b) {
    console.warn(a + b);
}
sum(100, 100);

var sum = function (a, b) {
    return a + b;
}
console.warn(sum(100, 200));